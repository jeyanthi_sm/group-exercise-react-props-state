interface chuckJokeProps {
	id:number;
	joke :string;
}
// An alternative way of declaring a component is to write it as a function which
// returns a React.ReactNode. This is equivalent to the syntax in <ChuckCard/>
function ChuckJoke(inputChuckJokeProps:chuckJokeProps): React.ReactNode {
/*	return (
		<> 
		<p>{inputChuckJokeProps.joke}</p>
		</>
	)
*/	return (
		<p>{inputChuckJokeProps.joke}</p>
	) 
}

export default ChuckJoke;